# Sacha's test - Front End

Task
Your task is to create a replica of the Github issues page for the React repo.
https://github.com/facebook/react/issues
We are not expecting the whole page to be built and we use the same task for every level of developer so concentrate on whichever parts you think would best demonstrate your skills.
Please don’t spend any more than 2-3 hours on this task.
When finished please send through the resulting code with a Readme explaining how to run
your app.

Please also add any parts you left out/would have added if you had more time on the Readme.

## How to run it
```
npm install
npm run dev

then navigate to http://localhost:3000/facebook/react/issues
```

If you have something already running on localhost:3000, you can run client and server in 2 separate commands

```
T1
npm start
T2
npm run server
```

## How to run tests
```
npm test
```

## Stack
### Front-end
React (hooks)
Typescript

## Step 1 - Planning/Scoping

I started by analyzing the page and trying to scope it down to something that sounds reasonable to do in just a few hours.
I started by looking at the page and analyzing its structure. The main components visible (Header, main, nav, footer) made think
of a specific structure that I decided to replicate in the application.

```javascript
<App>
  // The header is always there
  <Header />
  // There is a repo page with several tabs
  <Repo>
    // Issues is one of those tabs
    <Issues />
  </Repo>
</App>
```

### SSR vs CSR
Github is server side rendered, and for that reason it doesn't show any loading screen, nor it is easy for me to directly use their API response properly.
For that reason I'm just creating the mock data I need to make the page look like the github one. I will use the same data for tests.

This is a schema resembling how it's supposed to work. This is obviously not production ready, just something that works for what
we're trying to achieve here.

I created a repos collection, which holds the data concerning one repository, and an issues collection that holds the data concerning each one of the specific issues.

```json
{
    "repos": {
      "repo-name": {
        "id": "number",
        "name": "string",
        "watching": "number",
        "user": "name",
        "issues-number": "number",
        "labels": [
          "Labels"
        ]
      },
    "issues": [
      {
        "issue-number": "string",
        "author": "number",
        "opened": "date",
        "title": "string",
        "labels": [
          "Labels"
        ],
        "messages": "number"
      }
    ]
  }
}
```

The idea is to fetch the Repo first (through name and userName) and then get the issues corresponding to the repo id.
Normally, when rendering client side, we use loaders/spinners, and my useFetch has an isLoading property that could help us doing that
but I decided not to do it because that would change the way the UI looks. I am happy to discuss ways that could be done.

### What is there and why
* The UI should resemble the page as much as possible, but the only functionality present should be the one concerning
fetching the data and showing it properly
* Routing should be present, although just as a POC
* No sorting/filtering
* The page should be reasonably responsive, but not tablet/mobile ready. It should cover all common resolution width from ~1100px.
* Unit tests for data fetching and for some UI components

### What is not there
* Login/account features
* None of the buttons works
* Though all the sections should be visible, all the links will redirect to the real page on github
* Sorting and filtering will be severely limited
* No dropdown menu for the various summary tags
* Nothing related to pagination
* No mobile version

## Step 2 - Building it

### Preparation phase
I started by setting up the project. First CRA with TS, then I added the packages I thought I would need.
```text
json-server
react-router-dom
styled-components
styled-normalize
concurrently
history
```

Then I moved to the setup. This was a mix of stuff that I built in the past (like the hooks in common)
and stuff that I knew I could wire up fast because I've done it before (styled components)

I thought a lot about implementing a full typography system and theme, but I decided not too. This translates in some repetition
when working with fonts, but I didn't have enough information on the font-size/weight/line-height I would need in advance.
I did create a color file with all the color constants, since I decided to go with CSS variables. This is because GitHub offers
a dark mode and having variables would make it much easier to achieve that in the future.
The color names are far from ideal, but I lacked enough information to give them more meaningful names.

### Hands on the code
I decided to build components based on their hierarchical order in App.tsx. That means I started with what belonged to the App component itself,
Header and Footer, before moving down to Repo and then, finally, to Issues.
The Header was particularly time-consuming, having several pieces that required unique styling.
The Footer was much simpler, being nothing more than two lists and a logo.
The Repo required some extra work and was the first one to use the mock API I created with json-server. It currently lacks all kind of error handling,
for time constraints
I started with the top links and the three buttons on the right, which I called "double link". There are 2 variations, the normal one and the small one.
Then I moved to the nav list at the bottom of the repo bar. Pretty simple, though I didn't spend too much time to make sure it reflects the selected tab and
for simplicity it always highlights issues. That said, it would be possible to make it work properly with a few changes.

After that, I moved to the final part, the actual issues section.
I started with the top bar, a simple form with one double-link and a button on the right.

Then, finally, the top of the issues section and the issues themselves.
I decided not to wire up the open/closed buttons, so currently they simply redirect to the correspondent page on github, but it wouldn't be too hard to do so
Same goes for the date logic to write how long ago an issue was opened. I decided not to, but I'm providing a date string in the fake api.
For the same reason, I ended up leaving out the user icons. I did retain the messages.

I wanted initially to use the search bar for filtering, but I decided not too because of time constraints.
I'm more than happy to discuss the different ways it could be done.

At this point I consider the application complete, having achieved the goal of "copying" GitHub's issues page.

## Step 3 - Tests

### What I decided to test
That every component renders correctly
Everything that has to do with Data Fetching (Repo/Issues), making sure elements show up correctly/show the correct numbers

The tests that involve fetching data are implemented by mocking fetch and its response
Since the application lacks error handling and loading, there aren't many test cases but even here I'm happy to discuss possible
solutions.