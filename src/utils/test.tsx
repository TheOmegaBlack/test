import {render, RenderOptions} from '@testing-library/react';
import { Router } from 'react-router-dom';
import React, {ComponentType, ReactChildren} from 'react';
import { createMemoryHistory } from 'history';

type Options = Omit<RenderOptions, 'queries'> & {
  initialState?: any,
}

export const renderWithRouter = (Component: React.ReactComponentElement<any>, options?: Options) => {
  const history = createMemoryHistory();

  const wrapper = ({ children }: { children: ReactChildren }) => (
    <Router history={history}>{children}</Router>
  );

  const renderedProps = render(Component, { wrapper: wrapper as ComponentType });

  return {
    ...renderedProps,
    history,
  };
};
