import { screen, render } from '@testing-library/react';

import {
  LeftLink,
  RightLink,
  SmallLeftLink,
  SmallRightLink,
} from './DoubleLink';

describe('DoubleLink', () => {
  describe('rendering', () => {
    it('renders correctly', () => {
      const Component = (
        <>
          <LeftLink href="test.com">Hello Left!</LeftLink>
          <RightLink href="test.com">Hello Right!</RightLink>
          <SmallLeftLink href="test.com">Hello Small Left!</SmallLeftLink>
          <SmallRightLink href="test.com">Hello Small Right!</SmallRightLink>
        </>
      );
      render(Component);

      const links = screen.getAllByRole('link');

      expect(links).toHaveLength(4);
    });

  });
});
