import styled, { css } from 'styled-components';

const Link = css`
  align-items: center;
  border: 1px solid var(--colors-doubleLinkBorder);
  color: var(--colors-veryDarkBlue);
  background-color: var(--colors-white);
  display: inline-flex;
  height: 32px;
  font-size: 14px;
  font-weight: 500;
  padding: 5px 16px;
  text-decoration: none;
  
  &:hover {
    background-color: var(--colors-ligthGrayishBlueDoubleLink);
  }
`;

export const LeftLink = styled.a`
  ${Link};
  
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
`;

export const RightLink = styled.a`
  ${Link};
  
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
  border-left: none;
`;


const SmallLink = css`
  ${Link};
  
  font-size: 12px;
  height: 28px;
  padding: 3px 12px;
`

export const SmallLeftLink = styled(LeftLink)`
  ${SmallLink};

  background-color: var(--colors-lightGrayishBlueRepo);
`

export const SmallRightLink = styled(RightLink)`
  ${SmallLink};
  
  font-weight: 600;
  border-left: none;
  &:hover {
    background-color: var(--colors-white);
    color: var(--colors-strongBlue);
  }
`