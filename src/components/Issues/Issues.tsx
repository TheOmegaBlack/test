import * as React from 'react';
import {
  IssuesWrapper,
  IssuesTopBar,
  SearchForm,
  DoubleLinkContainer,
  GreenButton,
  IssuesContent,
  IssuesListHeader,
  StateLink,
  StateLinkContainer,
  FiltersContainer,
  Caret,
  IssueContainer,
  IssueDescription,
  IssueLabel,
  IssueInfo,
} from './styles';
import { LeftLink, RightLink } from '../DoubleLink';
import SearchIcon from '../../common/icons/SearchIcon';
import LabelIcon from '../../common/icons/LabelIcon';
import MilestoneIcon from '../../common/icons/MilestoneIcon';
import { LabelType } from '../Repo/Repo';
import IssueIcon from '../../common/icons/IssueIcon';
import { useFetch } from '../../common/hooks/hooks';
import { serverUrl } from '../../common/constants';
import MessageIcon from '../../common/icons/MessageIcon';
import CheckIcon from '../../common/icons/CheckIcon';

type IssuesPropsType = {
  repoId: number;
  milestones: number;
  labelsNumber: number;
  labels: LabelType[];
  issuesNumber: number;
  user: string;
  repo: string;
};

type IssueType = {
  repoId: number;
  id: number;
  issueNumber: number;
  author: string;
  opened: string;
  isOpen: boolean;
  title: string;
  labelIds: number[];
  messages: number;
};

type MemoType = {
  [key: number]: LabelType;
};

type GetRightLabelType = {
  memo: MemoType;
} & ((labels: LabelType[], labelId: number) => LabelType | undefined);

const getRightLabel: GetRightLabelType = (labels, labelId) => {
  const { memo } = getRightLabel;

  if (memo[labelId]) {
    return memo[labelId];
  } else {
    const label = labels.find((l) => labelId === l.id);
    memo[labelId] = label as LabelType;
    return label;
  }
};

getRightLabel.memo = {};

const Issues = (props: IssuesPropsType) => {
  const {
    repoId,
    milestones,
    labelsNumber,
    issuesNumber,
    user,
    repo,
    labels,
  } = props;

  const [selectedIssue, setSelectedIssue] = React.useState<null | number>(null);
  const { data: issues, execute: fetchIssues } = useFetch<IssueType[]>([]);

  React.useEffect(() => {
    // If I were to implement the difference between open and closed issues
    // I would probably make a state variable that can be changed clicking on the links below
    // and that would cause this side-effect to reload changing the query to isOpen=false
    fetchIssues(`${serverUrl}/issues?repoId=${repoId}&isOpen=true`);
  }, [repoId]);

  return (
    <IssuesWrapper>
      <IssuesContent>
        <IssuesTopBar>
          <SearchForm>
            <SearchIcon />
            <input />
          </SearchForm>
          <DoubleLinkContainer>
            <LeftLink href="https://github.com/facebook/react/labels">
              <LabelIcon />
              Labels
              <span>{labelsNumber}</span>
            </LeftLink>
            <RightLink href="https://github.com/facebook/react/milestones">
              <MilestoneIcon />
              Milestones
              <span>{milestones}</span>
            </RightLink>
          </DoubleLinkContainer>
          <GreenButton type="button">New Issue</GreenButton>
        </IssuesTopBar>
        <IssuesListHeader>
          <StateLinkContainer>
            <StateLink selected>
              <IssueIcon />
              {issuesNumber} Open
            </StateLink>
            <StateLink>
              <CheckIcon />0 Closed
            </StateLink>
          </StateLinkContainer>
          <FiltersContainer>
            <li>
              <summary>
                Author <Caret />
              </summary>
            </li>
            <li>
              <summary>
                Label <Caret />
              </summary>
            </li>
            <li>
              <summary>
                Projects <Caret />
              </summary>
            </li>
            <li>
              <summary>
                Milestones <Caret />
              </summary>
            </li>
            <li>
              <summary>
                Assignee <Caret />
              </summary>
            </li>
            <li>
              <summary>
                Sort <Caret />
              </summary>
            </li>
          </FiltersContainer>
        </IssuesListHeader>
        {issues.map((issue) => {
          const {
            issueNumber,
            id,
            author,
            title,
            messages,
            labelIds = [],
          } = issue;

          const issueLink = `https://www.github.com/${user}/${repo}/issues/${issueNumber}`;

          return (
            <IssueContainer
              key={id}
              onMouseEnter={() => setSelectedIssue(id)}
              selected={selectedIssue === id}
              data-testid="issueContainer"
            >
              <IssueIcon />
              <IssueDescription>
                <a href={issueLink}>{title}</a>
                {labelIds.map((labelId) => {
                  const label = getRightLabel(labels, labelId);

                  if (!label) return null;

                  return (
                    <IssueLabel
                      backgroundColor={label.backgroundColor}
                      color={label.color}
                      key={label.id}
                    >
                      {label.name}
                    </IssueLabel>
                  );
                })}
                <div>{`#${issueNumber} opened by ${author}`}</div>
              </IssueDescription>
              <IssueInfo>
                {!!messages && (
                  <a href={issueLink}>
                    <MessageIcon /> <span>{messages}</span>
                  </a>
                )}
              </IssueInfo>
            </IssueContainer>
          );
        })}
      </IssuesContent>
    </IssuesWrapper>
  );
};

export default Issues;
