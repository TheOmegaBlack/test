import styled, { css } from 'styled-components';

export const IssuesWrapper = styled.div`
  padding: 0 32px;
`;

export const IssuesContent = styled.div`
  margin: 0 auto;
  max-width: 1216px;
`;

export const IssuesTopBar = styled.div`
  display: flex;
  justify-content: space-between;
  height: 32px;
  margin-bottom: 16px;
  width: 100%;
`;

export const SearchForm = styled.form`
  flex: 1;
  height: 100%;
  max-width: 800px;
  position: relative;

  input {
    background-color: var(--colors-lightGrayishBlueRepo);
    border: 1px solid var(--colors-lightGrayishBlueBorder);
    border-radius: 6px;
    color: var(--colors-veryDarkGrayishBlue);
    font-size: 14px;
    height: 100%;
    padding: 5px 12px 5px 32px;
    width: 100%;

    &:focus {
      border-color: #0366d6;
      outline: none;
      box-shadow: 0 0 0 3px rgba(3, 102, 214, 0.3);
    }
  }

  svg {
    position: absolute;
    left: 8px;
    top: 9px;
  }
`;

export const DoubleLinkContainer = styled.div`
  margin-left: 16px;

  svg {
    margin-right: 6px;
  }

  span {
    background-color: var(--colors-lightGrayishBlueLogo50);
    border: 1px solid transparent;
    border-radius: 24px;
    color: var(--colors-veryDarkBlue);
    height: 18px;
    font-size: 12px;
    font-weight: 500;
    padding: 1px 6px;
    margin-left: 6px;
  }
`;

export const GreenButton = styled.button`
  background-color: var(--colors-limeGreenBtn);
  border: 1px solid var(--colors-veryDarkBlueBorder);
  border-radius: 6px;
  color: var(--colors-white);
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  height: 100%;
  padding: 1px 16px;
  margin-left: 16px;

  &:hover {
    background-color: var(--colors-limeGreenBtnHover);
  }
`;

export const IssuesListHeader = styled.div`
  background-color: var(--colors-ligthGrayishBlueDoubleLink);
  border: 1px solid var(--colors-veryDarkBlueBorder);
  border-radius: 6px 6px 0 0;
  display: flex;
  justify-content: space-between;
  height: 55px;
  padding: 16px;
  width: 100%;
`;

export const StateLinkContainer = styled.div`
  display: flex;
  align-items: center;
`;

type StateLinkType = {
  selected?: boolean;
};

export const StateLink = styled.a<StateLinkType>`
  ${({ selected }) => css`
    color: ${selected
      ? 'var(--colors-veryDarkBlue)'
      : 'var(--colors-veryDarkGrayishBlue)'};

    svg {
      margin-right: 8px;
      fill: ${selected
        ? 'var(--colors-veryDarkBlue)'
        : 'var(--colors-veryDarkGrayishBlue)'};
    }
  `}

  display: inline-flex;
  align-items: center;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  
  &:hover {
    color: var(--colors-veryDarkBlue);
    
    svg {
      fill: var(--colors-veryDarkBlue);
    }
  }

  &:last-child {
    margin-left: 10px;
  }
`;

export const Caret = styled.span`
  border-left: 4px solid transparent;
  border-right: 4px solid transparent;
  border-top: 4px solid var(--colors-veryDarkGrayishBlue);
  height: 0;
  margin-left: 4px;
  margin-top: 2px;
  width: 0;
`;

export const FiltersContainer = styled.div`
  display: flex;

  li {
    align-items: center;
    cursor: pointer;
    display: flex;
    list-style: none;
    padding: 0 16px;

    &:last-of-type {
      padding-right: 0;
    }

    summary {
      align-items: center;
      color: var(--colors-veryDarkGrayishBlue);
      display: flex;
      font-size: 14px;

      &:hover {
        color: var(--colors-veryDarkBlue);
        
        span {
          border-top: 4px solid var(--colors-veryDarkBlue);
        }
      }
    }
  }
`;

type IssueContainerType = {
  selected: boolean;
};

export const IssueContainer = styled.div<IssueContainerType>`
  background-color: ${({ selected }) =>
    selected
      ? 'var(--colors-ligthGrayishBlueDoubleLink)'
      : 'var(--colors-white)'};
  border: 1px solid var(--colors-veryDarkBlueBorder);
  border-top: none;
  display: flex;
  padding: 8px 16px 0;
  min-height: 63px;

  > svg {
    fill: var(--colors-DarklimeGreen);
  }
`;

export const IssueDescription = styled.div`
  flex-grow: 1;
  font-weight: 600;
  min-width: 0;
  padding: 0 8px 8px;

  a:first-child {
    color: var(--colors-veryDarkBlue);
    text-decoration: none;
    white-space: nowrap;
  }

  div {
    color: var(--colors-veryDarkGrayishBlue);
    line-height: 18px;
    font-size: 12px;
    font-weight: 400;
    margin-top: 4px;
  }
`;

type IssueLabelStyleType = {
  backgroundColor: string;
  color: string;
};

export const IssueLabel = styled.a<IssueLabelStyleType>`
  background-color: ${({ backgroundColor }) => backgroundColor};
  border: 1px solid ${({ backgroundColor }) => backgroundColor};
  color: ${({ color }) => color};
  cursor: pointer;
  border-radius: 24px;
  display: inline-block;
  line-height: 18px;
  margin-left: 5px;
  margin-top: 3px;
  font-size: 12px;
  padding: 0 7px;
  white-space: nowrap;
`;

export const IssueInfo = styled.div`
  flex: 0 0 25%;
  font-size: 12px;
  display: flex;
  justify-content: flex-end;

  a {
    color: var(--colors-veryDarkBlue);
    display: flex;
    margin-top: 4px;
    text-decoration: none;
    
    svg {
      margin-right: 4px;
    }
  }
`;
