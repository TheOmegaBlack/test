import { screen } from '@testing-library/react';
import { getByText } from '@testing-library/dom';
import { renderWithRouter } from '../../utils/test';
import database from '../../server/db.json';
import Issues from './Issues';
import * as React from 'react';

describe('People List', () => {
  let fetchMock: jest.SpyInstance;
  beforeAll(() => {
    fetchMock = jest.spyOn(window, 'fetch');
  });

  it('renders and shows data correctly', async () => {
    fetchMock.mockResolvedValueOnce({
      ok: true,
      status: 200,
      json: () => Promise.resolve(database.issues),
    });

    const repo = database.repos[0];

    renderWithRouter(
      <Issues
        repoId={1}
        labels={repo.labels}
        labelsNumber={repo.labelsNumber}
        milestones={repo.milestones}
        issuesNumber={repo.issuesNumber}
        user="facebook"
        repo="react"
      />,
    );

    const labelsLink = screen.getByRole('link', { name: /labels/i });
    const milestonesLink = screen.getByRole('link', { name: /milestones/i });
    const openIssuesLink = await screen.findByText(/open/i);
    const closedIssuesLink = screen.getByText(/closed/i);

    const issues = screen.getAllByTestId('issueContainer');

    expect(getByText(labelsLink, repo.labelsNumber)).toBeDefined();
    expect(getByText(milestonesLink, repo.milestones)).toBeDefined();
    expect(
      getByText(openIssuesLink, new RegExp(repo.issuesNumber.toString())),
    ).toBeDefined();
    expect(getByText(closedIssuesLink, /0/)).toBeDefined();
    expect(issues).toHaveLength(10);
  });
});
