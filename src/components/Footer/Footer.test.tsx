import { screen, render } from '@testing-library/react';
import { Footer } from './';

describe('Footer', () => {
  describe('rendering', () => {
    it('renders correctly', () => {
      render(<Footer />);

      const links = screen.getAllByRole('link');
      const trademark = screen.getByText('© 2021 GitHub, Inc.')

      expect(links).toHaveLength(12);
      expect(trademark).toBeDefined()
    });
  });
});
