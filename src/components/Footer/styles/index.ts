import styled from "styled-components";

export const FooterWrapper = styled.footer`
  position: relative;
  bottom: 0;
  height: 115px;
  width: 100%;
  margin-top: 40px;
  padding: 0 16px;
`
export const FooterContainer = styled.div`
  border-top: 1px solid var(--colors-lightGrayishBlue);
  display: flex;
  margin: 0 auto;
  padding-top: 40px;
  max-width: 1248px;
  justify-content: space-between;
`
export const FooterList = styled.ul`
  display: flex;
  flex: 1 1 520px;
  justify-content: space-between;
  list-style: none;
  max-width: 520px;
  padding: 0;
  margin: 0;
  
  li {
    font-size: 12px;
    color: var(--colors-veryDarkGrayishBlue);
    
    a {
      color: var(--colors-strongBlue);
      text-decoration: none;
      
      &:hover {
        text-decoration: underline; 
      }
    }
  }
`