import { TransparentLogo } from '../../common/icons/GitHubLogo';
import { FooterContainer, FooterList, FooterWrapper } from './styles';

const leftList = [
  {
    text: 'Terms',
    link: 'https://github.com/site/terms',
  },
  {
    text: 'Privacy',
    link: 'https://github.com/site/privacy',
  },
  {
    text: 'Security',
    link: 'https://github.com/security',
  },
  {
    text: 'Status',
    link: 'https://www.githubstatus.com/',
  },
  {
    text: 'Docs',
    link: 'https://docs.github.com/',
  },
];

const rightList = [
  {
    text: 'Contact GitHub',
    link: 'https://github.com/contact',
  },
  {
    text: 'Pricing',
    link: 'https://github.com/pricing',
  },
  {
    text: 'API',
    link: 'https://docs.github.com/',
  },
  {
    text: 'Training',
    link: 'https://services.github.com/',
  },
  {
    text: 'Blog',
    link: 'https://github.blog/',
  },
  {
    text: 'About',
    link: 'https://github.com/about',
  },
];

const Footer = () => (
  <FooterWrapper>
    <FooterContainer>
      <FooterList>
        <li>© 2021 GitHub, Inc.</li>
        {leftList.map(({ text, link }) => (
          <li key={text}>
            <a href={link}>{text}</a>
          </li>
        ))}
      </FooterList>
      <a href="https://github.com/"><TransparentLogo /></a>
      <FooterList>
        {rightList.map(({ text, link }) => (
          <li key={text}>
            <a href={link}>{text}</a>
          </li>
        ))}
      </FooterList>
    </FooterContainer>
  </FooterWrapper>
);

export default Footer;
