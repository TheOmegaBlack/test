import styled from 'styled-components';

export const HeaderWrapper = styled.header`
  display: block;
  background: var(--colors-veryDarkBlue);
  height: 72px;
  padding: 8px 0;
`;

export const ContentWrapper = styled.div`
  display: flex;
  height: 56px;
  max-width: 1280px;
  margin: 0 auto;
  align-items: center;
  padding: 0 16px;
`

export const LinkLogo = styled.a`
  margin-right: 24px;
`

export const NavWrapper = styled.div`
  display: flex;
  height: 100%;
  justify-content: space-between;
  width: 100%;
`

export const NavList = styled.ul`
  display: flex;
  height: 100%;
  align-items: center;
  list-style: none;
  margin: 0;
  padding: 0;
  color: var(--colors-white);
  
  li {
    margin-right: 16px;
    
    a {
      text-decoration: none;
      color: var(--colors-white);
      transition: opacity 400ms ease;
      
      &:hover {
        opacity: 70%;
        transition: opacity 400ms ease;
      }
    }
    
    summary {
      cursor: default;
      color: var(--colors-white);
      transition: opacity 400ms ease;
      
      svg {
        stroke: var(--colors-opaqueWhite50);
        margin-left: 5px;
        transition: stroke 400ms ease;
      }
  
      &:hover {
        opacity: 70%;
        transition: opacity 400ms ease;
        
        svg {
          stroke: var(--colors-opaqueWhite75);
          transition: stroke 400ms ease;
        }
      }
    }
  }
`

export const RightContent = styled.div`
  display: flex;
  align-items: center;
  
  > a {
    color: var(--colors-white);
    margin-right: 16px;
    text-decoration: none;

    &:hover {
      opacity: 70%;
      transition: opacity 400ms ease;
    }
    
    &:last-of-type {
      border: 1px solid var(--colors-white);
      border-radius: 6px;
      margin-right: 0;
      height: 34px;
      padding-top: 6px;
      text-align: center;
      vertical-align: middle;
      width: 72px;
    }
  };
`

export const SiteSearchForm = styled.form`
  width: 240px;
  height: 36px;
  display: flex;
  align-items: center;
  margin-right: 16px;
  
  label {
    height: 36px;
    display: flex;
    align-items: center;
    width: 100%
  }
  
  input:placeholder-shown {
    width: 213px;
    border: none;
    background: inherit;
    
    &::placeholder {
      opacity: 70%;
      color: #ffffff;
      font-size: 14px;
    }
  }
  
  input:focus {
    background: white;
    border: 0;
    border-radius: 5px 5px 3px 3px;
    height: 100%;
    outline: none;
    padding: 8px 12px;
    position: relative;
    width: 100%;
    
    & + svg {
      display: none;
    }
  }
`