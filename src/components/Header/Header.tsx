import * as React from 'react';
import GitHubLogo from '../../common/icons/GitHubLogo';
import {
  HeaderWrapper,
  ContentWrapper,
  LinkLogo,
  NavWrapper,
  NavList,
  RightContent,
  SiteSearchForm
} from './styles';
import ThinArrowDown from '../../common/icons/ThinArrowDown';
import SearchIconSlash from '../../common/icons/SearchIconSlash';

const Header = () => (
  <HeaderWrapper>
    <ContentWrapper>
      <LinkLogo href="https://www.github.com">
        <GitHubLogo />
      </LinkLogo>
      <NavWrapper>
        <NavList>
          <li><summary>Why GitHub?<ThinArrowDown /></summary></li>
          <li><a href="https://github.com/team">Team</a></li>
          <li><a href="https://github.com/enterprise">Enterprise</a></li>
          <li><summary>Explore<ThinArrowDown /></summary></li>
          <li><a href="https://github.com/marketplace">Marketplace</a></li>
          <li><summary>Pricing<ThinArrowDown /></summary></li>
        </NavList>
        <RightContent>
          <SiteSearchForm>
            <label><input placeholder="Search" /><SearchIconSlash /></label>
          </SiteSearchForm>
          <a href="https://github.com/login">Sign in</a>
          <a href="https://github.com/join">Sign up</a>
        </RightContent>
      </NavWrapper>
    </ContentWrapper>
  </HeaderWrapper>
);

export default Header;
