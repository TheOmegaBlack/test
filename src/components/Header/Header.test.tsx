import { screen, render } from '@testing-library/react';
import { Header } from './';

describe('Header', () => {
  describe('rendering', () => {
    it('renders correctly', () => {
      render(<Header />);

      const links = screen.getAllByRole('link');
      const input = screen.getAllByRole('textbox')


      expect(links).toHaveLength(6);
      expect(input).toBeDefined()
    });
  });
});
