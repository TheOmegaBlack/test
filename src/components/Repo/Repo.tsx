import * as React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
// I'm aware these paths are ugly, but CRA doesn't provide any way of doing it out of the box
// and I decided to avoid extra configuration
import { serverUrl } from '../../common/constants';
import { useFetch } from '../../common/hooks/hooks';
import { SmallLeftLink, SmallRightLink } from '../DoubleLink';
import RepoIcon from '../../common/icons/RepoIcon';
import WatchIcon from '../../common/icons/WatchIcon';
import StarIcon from '../../common/icons/StarIcon';
import ForkIcon from '../../common/icons/ForkIcon';
import CodeIcon from '../../common/icons/CodeIcon';
import IssueIcon from '../../common/icons/IssueIcon';
import PRIcon from '../../common/icons/PRIcon';
import ActionIcon from '../../common/icons/ActionIcon';
import ProjectIcon from '../../common/icons/ProjectIcon';
import WikiIcon from '../../common/icons/WikiIcon';
import SecurityIcon from '../../common/icons/SecurityIcon';
import InsightIcon from '../../common/icons/InsightIcon';
import {
  Main,
  RepoBarWrapper,
  RepoTopBar,
  RepoHeading,
  RepoTopButtons,
  RepoBottomList,
  RepoNavTab,
} from './styles';
import Issues from '../Issues';

type RouteMatchType = { user: string; repo: string };

export type LabelType = {
  id: number;
  backgroundColor: string;
  color: string;
  name: string;
};

type RepoType = {
  name: string;
  id: number;
  user: {
    id: number;
    name: string;
  };
  issuesNumber: number;
  labelsNumber: number;
  labels: LabelType[];
  milestones: number;
  watchers: string;
  starred: string;
  forked: number;
  pullRequests: number;
};

const Repo = () => {
  const match = useRouteMatch<RouteMatchType>();
  const {
    params: { user, repo },
  } = match;

  const {
    data: repoResponse,
    execute: fetchRepo,
  } = useFetch<RepoType[]>([]);

  React.useEffect(() => {
    fetchRepo(`${serverUrl}/repos?name.user=${user}&name=${repo}`);
  }, [user, repo]);

  const [repoData] = repoResponse;

  return (
    <Main>
      <RepoBarWrapper>
        <RepoTopBar>
          <RepoHeading>
            <RepoIcon />
            <a href={`https://github.com/${user}`}>{user}</a>
            <span>/</span>
            <a href={`https://github.com/${user}/${repo}`}>{repo}</a>
          </RepoHeading>
          <RepoTopButtons>
            <li>
              <SmallLeftLink href="#">
                <WatchIcon />
                <span>Watch</span>
              </SmallLeftLink>
              <SmallRightLink href="https://github.com/facebook/react/watchers">
                {/*
                  This is here as a quickfix since I'm not handling loading state
                  Ideally it would show some kind of spinner until the component is in a fulfilled state
                */}
                {repoData?.watchers ?? ''}
              </SmallRightLink>
            </li>
            <li>
              <SmallLeftLink href="#">
                <StarIcon />
                <span>Star</span>
              </SmallLeftLink>
              <SmallRightLink href="https://github.com/facebook/react/stargazers">
                {/* Same as component above */}
                {repoData?.starred ?? ''}
              </SmallRightLink>
            </li>
            <li>
              <SmallLeftLink href="#">
                <ForkIcon />
                <span>Fork</span>
              </SmallLeftLink>
              <SmallRightLink href="https://github.com/facebook/react/network/members">
                {/* Same as components above */}
                {repoData?.forked ?? ''}
              </SmallRightLink>
            </li>
          </RepoTopButtons>
        </RepoTopBar>
        <nav>
          <RepoBottomList>
            <RepoNavTab>
              <a href="https://github.com/facebook/react">
                <CodeIcon />
                Code
              </a>
            </RepoNavTab>
            <RepoNavTab selected>
              <a href="https://github.com/facebook/react/issues">
                <IssueIcon />
                Issues
                {/* Same as components above */}
                <span>{(repoData?.issuesNumber) || ''}</span>
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/pulls">
                <PRIcon />
                Pull requests
                {/* Same as components above */}
                <span>{repoData?.pullRequests || ''}</span>
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/actions">
                <ActionIcon />
                Actions
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/projects">
                <ProjectIcon />
                Projects
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/wiki">
                <WikiIcon />
                Wiki
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/security">
                <SecurityIcon />
                Security
              </a>
            </RepoNavTab>
            <RepoNavTab>
              <a href="https://github.com/facebook/react/pulse">
                <InsightIcon />
                Insights
              </a>
            </RepoNavTab>
          </RepoBottomList>
        </nav>
      </RepoBarWrapper>
      <Switch>
        <Route path={match.url + '/issues'}>
          {/*
            Similar to the components above, it shouldn't be rendered until it has the props it needs
          */}
          <Issues
            repoId={repoData?.id || 0}
            labels={repoData?.labels || []}
            labelsNumber={repoData?.labelsNumber || 0}
            milestones={repoData?.milestones || 0}
            issuesNumber={repoData?.issuesNumber || 0}
            user={user}
            repo={repo}
          />
        </Route>
      </Switch>
    </Main>
  );
};

export default Repo;
