import { screen } from '@testing-library/react';
import { getByText } from '@testing-library/dom';
import { renderWithRouter } from '../../utils/test';
import Repo from './Repo';
import database from '../../server/db.json';

describe('People List', () => {
  let fetchMock: jest.SpyInstance
  beforeAll(() => {
    fetchMock = jest.spyOn(window, 'fetch');
  });

  it('renders and shows data correctly', async () => {
    fetchMock.mockResolvedValueOnce({
      ok: true,
      status: 200,
      json: () => Promise.resolve(database.repos),
    });

    renderWithRouter(<Repo />);

    const watchLink =  await screen.findByRole('link', { name: database.repos[0].watchers })
    const starredLink =  screen.getByRole('link', { name: database.repos[0].starred })
    const forkLink =  screen.getByRole('link', { name: database.repos[0].forked })
    const issuesLink =  screen.getByRole('link', { name: /Issues/i })
    const prLink =  screen.getByRole('link', { name: /Pull requests/i })

    expect(watchLink).toBeDefined()
    expect(starredLink).toBeDefined()
    expect(forkLink).toBeDefined()
    expect(getByText(issuesLink, database.repos[0].issuesNumber)).toBeDefined()
    expect(getByText(prLink, database.repos[0].pullRequests)).toBeDefined()
  });
})