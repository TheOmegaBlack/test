import styled, { css } from 'styled-components';
import { ReactNode } from 'react';

export const Main = styled.main`
  height: calc(100vh - 72px - 155px);
`

export const RepoBarWrapper = styled.div`
  background-color: var(--colors-lightGrayishBlueRepo);
  padding-top: 16px;
  margin-bottom: 32px;
  width: 100%;
`;

export const RepoTopBar = styled.div`
  align-items: center;
  justify-content: space-between;
  display: flex;
  height: 32px;
  margin-bottom: 16px;
  padding: 0 32px;
  width: 100%;
`;

export const RepoHeading = styled.h1`
  font-size: 20px;
  height: 32px;
  display: flex;
  align-items: center;
  font-weight: 400;

  svg {
    margin-right: 8px;
  }

  a {
    text-decoration: none;
    color: var(--colors-strongBlue);

    &:hover {
      text-decoration: underline;
    }

    &:last-of-type {
      font-weight: 600;
    }
  }

  span {
    color: var(--colors-veryDarkGrayishBlue);
    margin: 0 4px;
  }
`;

export const RepoTopButtons = styled.ul`
  align-items: center;
  display: flex;
  flex-grow: 0;
  padding: 2px 0;

  li {
    display: flex;
    list-style: none;
    margin-right: 10px;

    &:last-child {
      margin: 0;
    }

    span {
      margin-top: -1px;
    }

    svg {
      margin-right: 6px;
    }
  }
`;

export const RepoBottomList = styled.ul`
  border-bottom: 1px solid var(--colors-lightGrayishBlueBorder);
  color: var(--colors-veryDarkBlue);
  display: flex;
  font-size: 14px;
  height: 48px;
  padding: 0 32px;
  position: relative;
  width: 100%;
`;

const RepoNavTabWrapper = styled.li<{ selected: boolean }>`
  ${({ selected }) => css`
    border-bottom: ${selected ? '2px solid var(--colors-softRed)' : 'none'};

    padding: 8px 16px;

    &:hover {
      border-bottom: ${selected
        ? '2px solid var(--colors-softRed)'
        : '2px solid var(--colors-lightGrayishBlueBorder)'};

      a {
        ${selected ? 'auto' : 'padding-top: 2px'};
      }
    }

    a {
      align-items: center;
      color: var(--colors-veryDarkBlue);
      display: flex;
      font-weight: ${selected ? '600' : 'auto'};
      height: 100%;
      text-decoration: none;
    }

    svg {
      margin-right: 6px;
      fill: ${selected
        ? 'var(--colors-veryDarkGrayishBlue)'
        : 'var(--colors-darkGrayishBlueIcon)'};
    }
  `}

  align-items: center;
  display: flex;
  list-style: none;
  position: relative;
  top: 1px;

  span {
    background-color: var(--colors-lightGrayishBlueLogo50);
    border: 1px solid transparent;
    border-radius: 24px;
    color: var(--colors-veryDarkBlue);
    height: 18px;
    font-size: 12px;
    font-weight: 500;
    padding: 1px 6px;
    margin-left: 6px;
  }
`;

type RepoNavTabPropsType = {
  children: ReactNode;
  selected?: boolean;
};

export const RepoNavTab = ({
  selected = false,
  children,
}: RepoNavTabPropsType) => (
  <RepoNavTabWrapper selected={selected}>{children}</RepoNavTabWrapper>
);
