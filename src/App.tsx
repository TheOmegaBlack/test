import { GlobalStyles } from './style';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Header } from './components/Header';
import styled from 'styled-components';
import { Footer } from './components/Footer';
import {Repo} from "./components/Repo";

const PageContainer = styled.div`
  position: relative;
  min-height: 100vh;
`

function App() {
  return (
    <>
      <GlobalStyles />
      <PageContainer>
        <Header />
        <BrowserRouter>
          <Switch>
            <Route path="/:user/:repo/">
                <Repo />
            </Route>
          </Switch>
        </BrowserRouter>
        <Footer />
      </PageContainer>
    </>
  );
}

export default App;
