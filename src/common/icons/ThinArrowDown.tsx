const ThinArrowDown = () => (
  <svg x="0px" y="0px" width="12px" viewBox="0 0 14 8" fill="none">
    <path fillRule="evenodd"  d="M1,1l6.2,6L13,1"/>
  </svg>
)

export default ThinArrowDown