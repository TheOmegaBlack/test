import * as React from 'react'

const statusEnum = {
    idle: 'IDLE',
    pending: 'PENDING',
    resolved: 'RESOLVED',
    rejected: 'REJECTED',
}

export const useAsyncAction = <Data>(initialState: Data) => {
    const [status, setStatus] = React.useState(statusEnum.idle)
    const [data, setData] = React.useState(initialState)
    const [error, setError] = React.useState<null | Error>(null)

    const execute = React.useCallback((promise: Promise<Data>): Promise<Error | Data> => {
        setStatus(statusEnum.pending)
        return promise
            .then(
                (data: Data) => {
                    setData(data)
                    setStatus(statusEnum.resolved)
                    return data
                },
                (error: Error) => {
                    setError(error)
                    setStatus(statusEnum.rejected)
                    return error
                },
            )
    }, [])

    return {
        execute,
        data,
        error,
        isIdle: status === statusEnum.idle,
        isLoading: status === statusEnum.pending,
        isSuccess: status === statusEnum.resolved,
        isError: status === statusEnum.rejected,
    }
}

export const useFetch = <Data>(initialState: Data) => {
    const { execute: ex, ...rest } = useAsyncAction<Data>(initialState)

    const execute = React.useCallback(
        (url: Request | string, options: RequestInit | undefined = {}) => ex(
            fetch(url, options)
                .then((res) => {
                    if (res.status >= 400) {
                        throw new Error('There was a problem')
                    }
                    return res.json()
                })
                .catch(
                    e => e
                ),
        ),
        [ex],
    )

    return {
        execute,
        ...rest,
    }
}
