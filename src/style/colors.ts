import { css } from 'styled-components';

const colors = css`
  --colors-lightGrayishBlue: #eaecef;
  --colors-lightGrayishBlueBorder: #e1e4e8;
  --colors-lightGrayishBlueRepo: #fafbfc;
  --colors-lightGrayishBlueLogo: #d1d5da;
  --colors-lightGrayishBlueLogo50: #d1d5da50;
  --colors-ligthGrayishBlueDoubleLink: #f6f8fa;
  --colors-limeGreenBtnHover: #2c974b;
  --colors-limeGreenBtn: #2ea44f;
  --colors-DarklimeGreen: #22863a;
  --colors-darkGrayishBlue: #6a737d;
  --colors-darkGrayishBlueIcon: #959da5;
  --colors-veryDarkGrayishBlue: #586069;
  --colors-veryDarkBlue: #24292e;
  --colors-veryDarkBlueBorder: #1b1f2315;
  --colors-doubleLinkBorder: #1b1f2315;
  --colors-softRed: #f9826c;
  --colors-strongBlue: #0366d6;
  --colors-opaqueWhite50: #ffffff50;
  --colors-opaqueWhite75: #ffffff75;
  --colors-white: #ffffff;
`;

export default colors;